<?php get_header(); ?>

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
		
		<?php if( have_posts() ) : while( have_posts() ): ?>
			<?php the_post(); ?>
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
