<footer class="footer container">
	<!-- <?php dynamic_sidebar('twitter'); ?> -->
	<div class="col-md-12 text-center">
		<?php echo date('Y'); ?>
		<a href="<?php echo home_url(); ?>"><?php bloginfo('name');?></a>
		<?php _e('All rights reserved.', 'New North'); ?>
	</div>
	
</footer>

<!-- End Document -->

<?php wp_footer(); ?>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
	(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
	function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
	e=o.createElement(i);r=o.getElementsByTagName(i)[0];
	e.src='//www.google-analytics.com/analytics.js';
	r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
	ga('create','UA-XXXXX-X');ga('send','pageview');
</script>

	
</body>
</html>